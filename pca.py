import sys

import pandas as pd
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
from sklearn.metrics import f1_score
from sklearn.ensemble import BaggingClassifier
pd.options.mode.chained_assignment = None

def preprocess_data(data):
    data = data.dropna()
    data.loc[:, 'jastuk'] = data['jastuk'].replace({'da': -1, 'ne': 1})
    data.loc[:, 'pojas'] = data['pojas'].replace({'da': -1, 'ne': 1})
    data.loc[:, 'pol'] = data['pol'].replace({'m': -1, 'z': 1})
    data.loc[:, 'tip'] = data['tip'].replace({'vozac': -1, 'putnik': 1})
    data.loc[:, 'ceoni'] = data['ceoni'].replace({0: -1, 1: 1})

    return data


if __name__ == '__main__':
    if len(sys.argv) > 1:
        train_path = sys.argv[1]
        test_path = sys.argv[2]
    else:
        train_path = "train.csv"
        test_path = "test_preview.csv"
    df_train = pd.read_csv(train_path)
    df_test = pd.read_csv(test_path)

    df_train = preprocess_data(df_train)
    X_train = df_train.drop('ishod', axis=1)
    Y_train = df_train['ishod']

    df_test = preprocess_data(df_test)
    X_test = df_test.drop('ishod', axis=1)
    Y_test = df_test['ishod']

    pca = PCA(n_components='mle', whiten=True, svd_solver='auto')
    pca.fit(X_train)
    X_train_transformed = pca.transform(X_train)
    X_test_transformed = pca.transform(X_test)

    param_grid = {
        'n_estimators': [10, 20, 30],
        'bootstrap': [True, False]
    }
    model = BaggingClassifier()
    grid = GridSearchCV(model, param_grid, cv=5, scoring='f1_macro', n_jobs=-1)

    grid.fit(X_train_transformed, Y_train)
    y_pred = grid.predict(X_test_transformed)
    print(f1_score(Y_test, y_pred, average='macro'))
